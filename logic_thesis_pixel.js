var LpConfig = LpConfig || {};
var CanSendData = true;
var TimeGoinPage = ''
LpConfig.submit_button = LpConfig.submit_button || '.btn-submit-lp-form';
LpConfig.thankyou_url = LpConfig.thankyou_url || 'thankyou.html';
LpConfig.pixel_id = LpConfig.pixel_id || '';
LpConfig.sending_failure_message = LpConfig.sending_failure_message || 'Lá»—i Ä‘Äƒng kÃ½, vui lÃ²ng kiá»ƒm tra láº¡i!';

// Should do something or not
var actions = ['fill_form_data'];
for (var i=0; i<actions.length; i++){
  LpConfig['should_' + actions[i]] = LpConfig['should_' + actions[i]] == undefined ? true : LpConfig['should_' + actions[i]];
}

// Should validate or not
var fields = ['name', 'mobile', 'email', 'city', 'district'];
for (var i=0; i<fields.length; i++){
  LpConfig['should_validate_' + fields[i]] = LpConfig['should_validate_' + fields[i]] == undefined ? true : LpConfig['should_validate_' + fields[i]];
}



$(function () {
  // Help find parent of a node
  $.fn.find_nearest_parent_with_name = function (name) {
    var node = this;
    while (node && node.prop('tagName') && node.prop('tagName').toUpperCase() != name.toUpperCase()){
      node = node.parent();
    }
    return node;
  };
});
//kc


// Cookie stuff
var setCookie = function(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
};

// Bind click event to form submit button
function bind_submit_lp_button(button){
  console.log(14)
  if ($(button).length > 0){
    $(button).click(function(e){
      var form = find_form_for_button(button);
      if (form){
        if (CanSendData) {
          CanSendData = false;
          submit_lp_form(form);
          setTimeout(function () { CanSendData = true }, 3000)
          
        }
      }else{
        throw 'Lá»—i khÃ´ng tÃ¬m tháº¥y form';
      }
      e.preventDefault();
      return false;
    });
  }else{
    throw 'KhÃ´ng tÃ¬m tháº¥y button sumit form';
  }
}

function find_form_for_button(button){
  console.log(13);
  if (LpConfig.find_form_for_submit_button){
    return LpConfig.find_form_for_submit_button($(button));
  }else{
    // var node = $(button);
    // while (node && node.prop('tagName') && node.prop('tagName').toUpperCase() != name.toUpperCase()){
    //   node = node.parent();
    // }
    // return node;
    return $(button).find_nearest_parent_with_name('form');
  }
}

// Get form info and send data
function submit_lp_form(form){
  // Form data
  form_data = {
    source: 'landingpage',
    source_url: document.location.href,
    pixel_id: LpConfig.pixel_id,
    google_key: LpConfig.google_key,
    time_goin_page: TimeGoinPage
  };

  // Find all params within form
  $(form).find('input').each(function(index, input){
    var type  = $(input).attr('type');
    var key   = $(input).attr('name');
    var value = $(input).val();
    if (type != 'submit' && key.length > 0 && value.length > 0){
      form_data[key] = value;
    }
  });

  delete form_data.ga_code;

  $(form).find('select').each(function(index, select){
    var key   = $(select).attr('name');
    var value = $(select).find('option:selected').val();
    if (key.length > 0 && value.length > 0){
      form_data[key] = value;
    }
  });

  // Validate data
  if (LpConfig.should_validate_name && !validate_name(form_data.name)) {
    return;
  };
  if (LpConfig.should_validate_email && !validate_email(form_data.email, form_data)) {
    return;
  };
  if (LpConfig.should_validate_mobile && !validate_mobile(form_data.mobile, form_data)) return;
  if ($('[name=district]').length > 0) {
    if (LpConfig.should_validate_district && !validate_district(form_data.district)) return;
  }
  if ($('[name=city]').length > 0) {
    if (LpConfig.should_validate_city && !validate_city(form_data.city)) return;
  }

  // Build address
  build_address(form_data);
  if (LpConfig.should_get_data_after_submit) {
    LpConfig.thankyou_url = LpConfig.thankyou_url + '?name=' + form_data.name + '&email=' + form_data.email + '&mobile=' + form_data.mobile + '&new_price=' + form_data.new_price + '&address=' + form_data.address + '&cid=' + form_data.course_id;
  }

  var massoffer_regex = /_massoffer_/i
  var utm_medium = get_query_string('utm_medium');

  if (massoffer_regex.test(utm_medium)) {
     if (LpConfig.thankyou_url.indexOf('?') > 0) {
  LpConfig.thankyou_url = LpConfig.thankyou_url + '&utm_source=massoffer&course_id='+ form_data.course_id + '&course_name=' + form_data.course_name + '&course_price=' + form_data.new_price + '&url=' + window.location.href + '&name=' + form_data.name + '&email=' + form_data.email + '&mobile=' + form_data.mobile;
     } else {
  LpConfig.thankyou_url = LpConfig.thankyou_url + '?utm_source=massoffer&course_id='+ form_data.course_id + '&course_name=' + form_data.course_name + '&course_price=' + form_data.new_price + '&url=' + window.location.href + '&name=' + form_data.name + '&email=' + form_data.email + '&mobile=' + form_data.mobile;
     }
  }  
  sendata_to_edumall();
  // Send data
  // send_to_marol(form_data);
  // Track event
  // send_to_tracking_system(form_data);
  // Alert if success
  // disable_after_submit();
}

//  Send contact to Online Payment
function sendata_to_edumall(){
  console.log(7)
  $.ajax({
    type: "post",
    url: 'https://lp-payments.edumall.vn/contact/contact_lp',
    data: form_data,
    dataType: 'json',
    timeout: 5000,
    success: function(response) {
      send_event_pixel(form_data.pixel_id, 'CompleteRegistration');
      // console.log(response.id);
      // console.log(ga.getAll()[0].get('linkerParam'));
      var ga_code = "";
      if(is_defined_ga()){
        ga_code = ga.getAll()[0].get('linkerParam');		
      }
      window.location.replace('https://lp-payments.edumall.io/checkout/payment/' + response.id + (ga_code !== "" ? ("?" + ga_code) : ""));
    },
    error: function(){
      $.ajax({
        type: "post",
        url: 'https://amun.edumall.io/public/v02/contactc3s/internal_create',
        data: form_data,
        dataType: 'json',
        contentType: "application/json",
        success: function(response){
          on_sending_success();
        },
        error: function(e) {
          console.error(e)
        }
      })
    }
  });
}
//  End send contact to Online Payment

// Send event pixel
function send_event_pixel(pixel_id, event) {
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', pixel_id);
  if(event == "CompleteRegistration"){
    fbq("track", event, {
      value: form_data.new_price,
      currency: "VND"
    });
  }else{
    fbq('track', event);
  }  
}

function disable_after_submit() {
  var button = $(LpConfig.submit_button + '>button');
  var bg_color = $(button).css('background-color');
  var color = $(button).css('color');
  $(button).attr('disabled', 'disabled').css({
    opacity: '0.5',
    cursor: 'not-allowed'
  });
  $(button).hover(function() {
    $(this).css({
      backgroundColor: bg_color,
      color: color
    });
  })
}

// An abstract function

function validate_name(name){
  console.log(1)
  if ((name || '').length == 0){
    return false;
  }
  return true;
}

function validate_city(city) {
  console.log(5);
  if ((address || '').length == 0){
    alert('Vui lÃ²ng nháº­p tá»‰nh thÃ nh cá»§a báº¡n');
    CanSendData = true;
    return false;
  }
  return true;
}

function validate_email(email, form_data){
  console.log(2);
  if ((email || '').length == 0) {
    return false;
  } else {
    email = email.replace(/\ /g, '');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email)) {
      return false;
    } else {
      form_data.email = email;
    }
  }
  return true;
}

function validate_mobile(mobile, form_data){
  console.log(3);
  if ((mobile || '').length == 0){
    alert('Vui lÃ²ng nháº­p sá»‘ Ä‘iá»‡n thoáº¡i cá»§a báº¡n');
    CanSendData = true;
    return false;
  }else {
    mobile = mobile.replace(/\ /g, '');
    if (!mobile.match(/^[\+\(\)\.\d]+$/) || mobile.length > 11 || mobile.length < 10) {
      alert('Sá»‘ Ä‘iá»‡n thoáº¡i khÃ´ng há»£p lá»‡, vui lÃ²ng kiá»ƒm tra láº¡i. Sá»‘ Ä‘iá»‡n thoáº¡i di Ä‘á»™ng pháº£i dÃ i hÆ¡n 9 vÃ  nhá» hÆ¡n 11 kÃ½ tá»±.');
      CanSendData = true;
      return false;
    } else {
      form_data.mobile = mobile;
    }
  }
  return true;
}

function validate_city(city){
  if ((city || '').length == 0){
    alert('Vui lÃ²ng nháº­p thÃ nh phá»‘ nÆ¡i báº¡n sá»‘ng');
    CanSendData = true;
    return false;
  }
  return true;
}

function validate_district(district){
  console.log(4);
  if ((district || '').length == 0){
    alert('Vui lÃ²ng nháº­p quáº­n huyá»‡n nÆ¡i báº¡n sá»‘ng');
    CanSendData = true;
    return false;
  }
  return true;
}

function build_address(form_data){
  console.log(6);
  
  var address = (form_data.address || '');
  address += ',' + (form_data.district || '');
  address += ',' + (form_data.city || '');
  if (address.length > 0){
    form_data.address = address;
  }

  delete form_data.district;
  delete form_data.city;
}

// function send_to_marol(form_data){
//   // var api = '//edumall.vn/contactc3s/insert';
//   var api = '//marol.edumall.vn/contact/import';
//   // send_to_flow(form_data);
//   // send_form_data(api, form_data, function(){
//   //   send_to_flow(form_data);
//   // }, on_sending_failure);

//    send_form_data(api, form_data, on_sending_success, on_sending_failure);
// }

function send_to_flow(form_data){
  var api = '//flow.edumall.vn:8000/notify/c3/create';
  send_form_data(api, form_data, on_sending_success, on_sending_failure);
}

function send_form_data(api, form_data, success_callback, failure_callback){
  console.log(9)
  var request = $.ajax({
    url: api,
    type: 'post',
    data: form_data,
    success: function(){
      if(success_callback){
        success_callback.call();
      }
      return false;
    },
    error: function(){
      if(failure_callback){
        failure_callback.call();
      }
      return false;
    }
  });
  return false;
}

function on_sending_success(){
  window.location.href = LpConfig.thankyou_url;
}

function on_sending_failure(){
  alert(LpConfig.sending_failure_message);
}

function send_to_tracking_system(form_data){
  // Track to spymaster
  track_submit_form(form_data);

  // Track c3_cod, c3 to: Google Analytics
  // send_event_submit_to_ga(form_data);
}

function setup_tracking(){
  if(is_defined_symaster()) {
    Spymaster.setup("Pedia", "beta");
  } else {
    console.error('Lá»—i thiáº¿u script tracking');
  }
}

function track_c2(){
  if(is_defined_symaster()) {
    var course_id = find_form_param('course_id');
    if ((course_id || '').length == 0){
      throw 'Vui lÃ²ng thÃªm course_id vÃ o form';
    }else{
      var params = {
        'category': 'C2',
        'behavior': 'view',
        'target': course_id
      }
      Spymaster.track(params);
    }
  } else {
    console.error('Lá»—i thiáº¿u script tracking');
  }
}

// Truyen vao extras dang json
function track_submit_form(extras){
  console.log(10)
  if(is_defined_symaster()) {
    var params = {
      'category': extras.type,
      'behavior': 'submit',
      'target': extras.course_id,
      'extras': extras || {}
    };
    Spymaster.track(params);
  } else {
    console.error('Lá»—i thiáº¿u script tracking');
  }
}

function send_event_submit_to_ga(extras){
  if(is_defined_ga()) {
    var ga_code = find_form_param('ga_code');
    if ((ga_code || '').length > 0){
      ga('send', 'event', extras.type, 'submit', ga_code || '');
    }else{
      throw 'Vui lÃ²ng thÃªm ga_code vÃ o form';
    }
  } else {
    console.error('Lá»—i thiáº¿u script tracking ga');
  }
}

function find_form_param(name){
  console.log(12)
  if ($(LpConfig.submit_button).length > 0){
    var form = find_form_for_button($(LpConfig.submit_button));
    if (form){
      return $(form).find('input[name=' + name + ']').val();
    }else{
      throw 'Lá»—i khÃ´ng tÃ¬m tháº¥y form';
    }
  }else{
    throw 'KhÃ´ng tÃ¬m tháº¥y button sumit form';
  }
}

// Landingpage provide form data
function _setup_form_submit(on_finish) {
  // var url_api = '//marol.edumall.vn/api/lp/get_data';
  var url_api = '//hera.edumall.io/api/public/v1/landing_pages/get_data';
  $.ajax({
    url: url_api,
    type: 'GET',
    data: {'domain': window.location.host},
    success: function(response) {
      if (response.status == 'ok') {
        $('form').each(function(i) {
          for (var field in response.data.fields) {
              if ($(this).find('input[name="' + field + '"]').length > 0) {
                if (response.data.fields[field] != '') {
                  $(this).find('input[name="' + field + '"]').val(response.data.fields[field]);
                }
              } else {
                if (response.data.fields[field] != '') {
                  $(this).prepend('<input type="hidden" name="' + field + '" value="'+ response.data.fields[field] +'"/>');
                } else {
                  $(this).prepend('<input type="hidden" name="' + field + '" value=""/>');
                }
              }
              for (var val in response.data.values) {
                if (response.data.values[val] != '') {
                  $('[lp-display="'+ val +'"]').text(response.data.values[val]);
                }
              };
          };
        });
      };
    },
    error: function(e) {
      console.log(e.message);
    }
  }).always(function(e) {
    if (on_finish) on_finish.call();
  });
}

function get_query_string(name) {
  name = name.replace(/[\[\]]/g, "\\$&");
  var url = window.location.href;
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
  if (!results) {
    return null;
  }
  if (!results[2]) {
    return '';
  }
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function is_defined_symaster() {
  console.log(11);
  return ((typeof Spymaster) == 'object')
}

function is_defined_ga(){
  console.log(8)
  return ((typeof ga) == 'function')
}

$(document).ready(function(){
  // Get time go in page
  TimeGoinPage = String(Date.now())

  // setup_tracking();

  if (LpConfig.should_fill_form_data){
    _setup_form_submit(function(){
      // track_c2();
    });
  } else {
    // track_c2();
  }

  // remove click event in submit_lp_button
  // getEventListeners($(LpConfig.submit_button).get(0)).click.forEach(i => $(LpConfig.submit_button).get(0).removeEventListener('click', i.listener))

  // bind event to submit button 
  bind_submit_lp_button(LpConfig.submit_button);
});
